/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var trigger = 1;
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        console.log("Bind event..");
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        
        //$('#app-data-source').hide();
        console.log("Device ready..");
        
        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);
        setTimeout(function(){ 
            navigator.splashscreen.hide(); 
            $('#app-data-source').show();
            //alert("Loading data.."); 
        }, 5000);

        function onOnline() {
            // Handle the online event
            //window.location.href = "index.html";
            //$('#app-data-source').show();
            //alert("App is now online..");
        };
        function onOffline() {
            // Handle the offline event
            $('#app-loader-gif').show();
            $('.outerloading').hide();
            $('#app-data-source').hide();
            $('#no-internet-div').show();
            console.log("Test");
            //window.location.href = "no_internet.html";
             
            if (confirm("No Internet Connection.")) {
                navigator.app.exitApp();
            } else {
                navigator.app.exitApp();
            }
            
        };
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        console.log('Received Event: ' + id);
    }
};

app.initialize();